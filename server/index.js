// console.log("Implement servermu disini yak 😝!");

const http = require('http');
const {
    PORT = 8000
} = process.env;

const fs = require('fs');
const path = require('path');
const PUBLIC_DIRECTORY = path.join(__dirname, '../public');
// console.log(PUBLIC_DIRECTORY);

function getHTML(htmlFileName) {
    const htmlFilePath = path.join(PUBLIC_DIRECTORY, htmlFileName);
    return fs.readFileSync(htmlFilePath, 'utf-8')
}

function onRequest(req, res) {
    if (req.url === "/") {
        res.writeHead(200)
        res.end(getHTML("index.html"))
        return;
    } 
    else if (req.url === "/cars") {
        res.writeHead(200)
        res.end(getHTML("cars.html"))
        return;
    }
    else if(req.url.match("\.css$")){
        var cssPath = path.join(PUBLIC_DIRECTORY, req.url);
        var fileStream = fs.createReadStream(cssPath, "UTF-8");
        res.writeHead(200, {"Content-Type": "text/css"});
        fileStream.pipe(res);
    }
    else if(req.url.match("\.png$")){
        var imagePath = path.join(PUBLIC_DIRECTORY, req.url);
        var fileStream = fs.createReadStream(imagePath);
        res.writeHead(200, {"Content-Type": "image/png"});
        fileStream.pipe(res);
    }
    else if(req.url.match("\.jpg$")){
        var imagePath = path.join(PUBLIC_DIRECTORY, req.url);
        var fileStream = fs.createReadStream(imagePath);
        res.writeHead(200, {"Content-Type": "image/jpg"});
        fileStream.pipe(res);
    }
    else if(req.url.match("\.svg$")){
        var imagePath = path.join(PUBLIC_DIRECTORY, req.url);
        var fileStream = fs.createReadStream(imagePath);
        res.writeHead(200, {"Content-Type": "image/svg+xml"});
        fileStream.pipe(res);
    }
    else if(req.url.match("\.js$")){
        var jsPath = path.join(PUBLIC_DIRECTORY, req.url);
        var fileStream = fs.createReadStream(jsPath);
        res.writeHead(200, {"Content-Type": "text/javascript"});
        fileStream.pipe(res);
    }
    else if(req.url.match("\.json$")){
        var jsonPath = path.join(PUBLIC_DIRECTORY, req.url);
        var fileStream = fs.createReadStream(jsonPath);
        res.writeHead(200, {"Content-Type": "application/json"});
        fileStream.pipe(res);
    }
     else {
        res.writeHead(404)
        res.end(getHTML("404.html"))
        return;
    }
}


const server = http.createServer(onRequest);

server.listen(PORT, 'localhost', () => {
    console.log("Server sudah berjalan, silahkan buka http://localhost:%d", PORT);
})