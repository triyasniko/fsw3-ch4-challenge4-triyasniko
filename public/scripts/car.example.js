class Car {
  static list = [];

  static init(cars) {
    this.list = cars.map((i) => new this(i));
  }

  constructor({
    id,
    plate,
    manufacture,
    model,
    image,
    rentPerDay,
    capacity,
    description,
    transmission,
    available,
    type,
    year,
    options,
    specs,
    availableAt,
  }) {
    this.id = id;
    this.plate = plate;
    this.manufacture = manufacture;
    this.model = model;
    this.image = image;
    this.rentPerDay = rentPerDay;
    this.capacity = capacity;
    this.description = description;
    this.transmission = transmission;
    this.available = available;
    this.type = type;
    this.year = year;
    this.options = options;
    this.specs = specs;
    this.availableAt = availableAt;
  }

  render() {
    return `
          <div class="col-md-4">
            <div class="card p-3" style="width: 18rem;">
                <img src="${this.image}" class="card-img-top" alt="..." />
                <div class="card-body">
                  <p class="fw-bold mt-1">${this.available}/${this.model}</p>
                  <h5 class="fw-bolder">Rp. ${this.rentPerDay} / hari</h5>
                  <p>${this.description} </p>
                  <div>
                  <span><i class="fa fa-user mr-3"></i>${this.capacity} Orang</span>
                  </div>
                  <div>
                  <span><i class="fa fa-gear mr-3"></i>${this.transmission}</span>
                  </div>
                  <div>
                  <span><i class="fa fa-calendar mr-3"></i>Tahun ${this.year}</span>
                  </div>

                  <div class="d-flex flex-column mt-3 align-items-stretch">
                    <button class="btn btn-success"> Pilih Mobil</button>
                  </div>
                </div>
            </div>
          </div>
    `;
  }
}
