class App {
  constructor() {
    this.clearButton = document.getElementById("clear-btn");
    this.loadButton = document.getElementById("load-btn");
    this.carContainerElement = document.getElementById("cars-container");
  }

  async init() {
    await this.load();

    // Register click listener
    this.clearButton.onclick = this.clear;
    this.loadButton.onclick = this.run;
  }

  run = () => {
    Car.list.forEach((car) => {
      const node = document.createElement("div");
      node.innerHTML = car.render();
      this.carContainerElement.appendChild(node);
    });
  };

  async load() {
    this.clear();
    const jumlahPenumpang=document.getElementById('jumlahPenumpang');
    const kapasitasPenumpang=jumlahPenumpang.value;
    console.log(kapasitasPenumpang);

    const cars = await Binar.listCars();
    // console.log(cars);
    // console.log(cars.length);
    const availableCar=cars.filter(function(car){
      return car.capacity>=kapasitasPenumpang&&car.available===true;
    })
    Car.init(availableCar);
  }

  clear = () => {
    let child = this.carContainerElement.firstElementChild;

    while (child) {
      child.remove();
      child = this.carContainerElement.firstElementChild;
    }
  };
}
